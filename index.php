<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PHP SC Activity 5</title>
</head>
<body>
        <?php session_start(); ?>

        <h1>Login</h1>
        <form method="POST" action="./server.php">
                Username: <input type="email" name="email" required><br/>
                Password: <input type="password" name="password" required><br/>
            <button type="submit">Login</button>
        </form>

        <?php if(isset($_SESSION['email'])): ?>
            <p> Hey! <?= $_SESSION['email']; ?></p>
        <?php else: ?>
            <?php if(isset ($_SESSION['login_error'])): ?>
                <p><?= $_SESSION['login_error']; ?></p>
            <?php endif; ?>
        <?php endif; ?>   
</body>
</html>